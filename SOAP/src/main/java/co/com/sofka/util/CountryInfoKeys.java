package co.com.sofka.util;

public enum CountryInfoKeys {

    COUNTRY_ISO_CODE("[stringCodigoCod]");

    private final String value;

    CountryInfoKeys(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }


}
