package co.com.sofka.question.countryinfo;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class TheCountryInfo implements Question<String> {


    @Override
    public String answeredBy(Actor actor) {
        return SerenityRest.lastResponse().asString();
    }

    public static TheCountryInfo theCountryInfo(){
        return new TheCountryInfo();
    }
}
