package co.com.sofka.question.countrycurrency;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class TheCurrencyName implements Question<String> {

    @Override
    public String answeredBy(Actor actor) {

        return SerenityRest.lastResponse().asString();
    }

    public static TheCurrencyName theCountryCurrency(){
        return new TheCurrencyName();
    }
}
