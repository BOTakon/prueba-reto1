package co.com.sofka.common;

import org.apache.log4j.PropertyConfigurator;

import java.util.HashMap;

import static co.com.sofka.util.Log4jValues.*;

public class ServiceSetup {
    protected static final String BASE_URI = "http://webservices.oorsprong.org";
    protected static final String RESOURCE = "/websamples.countryinfo/CountryInfoService.wso";

    protected static final String COUNTRY_CURRENCY_XML_FILE = "countrycurrency.xml";
    protected static final String LIST_OF_CONTINENTS_XML_FILE = "listofcontinents.xml";
    protected static final String CAPITAL_CITYS_XML_FILE = "capitalcity.xml";
    protected static final String COUNTRY_INFO_XML_FILE = "countryinfo.xml";

    protected static HashMap<String, Object> headers = new HashMap<>();
    protected static final String SOAP_ACTION = "SOAPAction";
    protected static final String CONTENT_TYPE = "Content-Type";

    protected String bodyRequest;
    private String SO = System.getProperty("os.name").toLowerCase();
    protected void generalSetUp(){
        setUpLog4j2();
    }

    private void setUpLog4j2(){
        if (SO.contains("win")) {

            PropertyConfigurator.configure(USER_DIR.getValue() + LOG4J_PROPERTIES_FILE_PATH_WINDOWS.getValue());
        }
        else {
            PropertyConfigurator.configure(USER_DIR.getValue() + LOG4J_PROPERTIES_FILE_PATH_LINUX.getValue());
        }
    }


}
