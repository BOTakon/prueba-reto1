package co.com.sofka.stepdefinitions.countryinfo;

import co.com.sofka.common.ServiceSetup;
import co.com.sofka.util.FileReader;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.apache.log4j.Logger;

import java.nio.charset.StandardCharsets;

import static co.com.sofka.question.countryinfo.TheCountryInfo.theCountryInfo;
import static co.com.sofka.task.DoPost.doPost;
import static co.com.sofka.util.CountryInfoKeys.COUNTRY_ISO_CODE;
import static co.com.sofka.util.Dictionary.EMPTY_STRING;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.apache.http.entity.ContentType.TEXT_XML;
import static org.hamcrest.CoreMatchers.containsString;

public class CountryInfoStepDefinition extends ServiceSetup {

    private static final Logger LOGGER = Logger.getLogger(CountryInfoStepDefinition.class);
    private Actor actor = Actor.named("JesusC");

    @Dado("que el usuario necesita consultar la informacion completa de un pais, con el codigo ISO")
    public void queElUsuarioNecesitaConsultarLaInformacionCompletaDeUnPaisConElCodigoISO() {
        try {
            generalSetUp();

            actor.can(CallAnApi.at(BASE_URI));
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage(), e);
        }
    }


    @Cuando("el usuario mediante el codigo ISO {string} ejecuta una consulta")
    public void elUsuarioMedianteElCodigoISOEjecutaUnaConsulta(String codigoISO) {
        try {
            FileReader reader = new FileReader(COUNTRY_INFO_XML_FILE);
            bodyRequest = reader.readContent().replace(COUNTRY_ISO_CODE.getValue(), codigoISO);
            headers.put(CONTENT_TYPE, TEXT_XML.withCharset(StandardCharsets.UTF_8).toString());
            headers.put(SOAP_ACTION, EMPTY_STRING);
            actor.attemptsTo(
                    doPost().
                            usingThe(RESOURCE).
                            with(headers).
                            and(bodyRequest)
            );
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage(), e);
        }
    }


    @Entonces("el ususario deberia obtener toda la informacion del pais que esta consultando")
    public void elUsusarioDeberiaObtenerTodaLaInformacionDelPaisQueEstaConsultando() {
        try {
            actor.should(
                    seeThatResponse(
                            "La respuesta HTTP es: " + SC_OK,
                            response -> response
                                    .statusCode(SC_OK)
                    ),
                    seeThat(
                            "Que la informacion del pais: ",
                            theCountryInfo(),
                            containsString("Colombia")
                    )
            );
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage(), e);
        }


    }


    @Dado("que el usuario ingresa un codigo ISO no valido {string}")
    public void queElUsuarioIngresaUnCodigoISONoValido(String codigoISOIncorrecto) {
        try {
            generalSetUp();

            FileReader reader = new FileReader(COUNTRY_INFO_XML_FILE);
            bodyRequest = reader.readContent().replace(COUNTRY_ISO_CODE.getValue(), codigoISOIncorrecto);
            headers.put(CONTENT_TYPE, TEXT_XML.withCharset(StandardCharsets.UTF_8).toString());
            headers.put(SOAP_ACTION, EMPTY_STRING);
            actor.can(CallAnApi.at(BASE_URI));
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Cuando("el usuario realiza una consulta desde la pagina")
    public void elUsuarioRealizaUnaConsultaDesdeLaPagina() {
        try {
            actor.attemptsTo(
                    doPost().
                            usingThe(RESOURCE).
                            with(headers).
                            and(bodyRequest)
            );
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage(), e);
        }

    }


    @Entonces("el usuario deberia obtener como respuesta: {string}")
    public void elUsuarioDeberiaObtenerComoRespuesta(String mensaje) {
        try {
            actor.should(
                    seeThatResponse(
                            "La respuesta HTTP es: " + SC_OK,
                            response -> response
                                    .statusCode(SC_OK)
                    ),
                    seeThat(
                            "Que el mensaje de la respuesta ",
                            theCountryInfo(),
                            containsString(mensaje)
                    )
            );
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage(), e);
        }
    }
}
