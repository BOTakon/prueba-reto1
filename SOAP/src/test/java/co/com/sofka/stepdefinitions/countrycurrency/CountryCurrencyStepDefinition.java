package co.com.sofka.stepdefinitions.countrycurrency;

import co.com.sofka.common.ServiceSetup;
import co.com.sofka.util.FileReader;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.hamcrest.Matchers;

import java.nio.charset.StandardCharsets;

import static co.com.sofka.question.countrycurrency.TheCurrencyName.theCountryCurrency;
import static co.com.sofka.question.TheResponseCode.theResponseCodeIs;
import static co.com.sofka.task.DoPost.doPost;
import static co.com.sofka.util.CountryCurrencyKeys.COUNTRY_ISO_CODE;
import static co.com.sofka.util.Dictionary.EMPTY_STRING;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.apache.http.entity.ContentType.TEXT_XML;

public class CountryCurrencyStepDefinition extends ServiceSetup {

    private static final Logger LOGGER = Logger.getLogger(CountryCurrencyStepDefinition.class);

    private final Actor theActor = Actor.named("Jose, el inversionista");


    @Dado("que el inversionista paso el codigo ISO del pais Colombia como {string} al recurso")
    public void queElInversionistaPasoElCodigoISODelPaisColombiaComoAlRecurso(String countryISOCode) {

        try {
            generalSetUp();
            FileReader reader = new FileReader(COUNTRY_CURRENCY_XML_FILE);
            bodyRequest = reader.readContent().replace(COUNTRY_ISO_CODE.getValue(), countryISOCode);
            LOGGER.info("El cuerpo del request es:\n "+bodyRequest);
            headers.put(CONTENT_TYPE, TEXT_XML.withCharset(StandardCharsets.UTF_8).toString());
            headers.put(SOAP_ACTION, EMPTY_STRING);

            theActor.can(CallAnApi.at(BASE_URI));
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage(),e);
        }
    }

    @Cuando("solicite el codigo ISO y el nombre de la moneda de ese pais")
    public void soliciteElCodigoISOYElNombreDeLaMonedaDeEsePais() {

        try {
            theActor.attemptsTo(
                    doPost()
                            .usingThe(RESOURCE)
                            .with(headers)
                            .and(bodyRequest)
            );
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage(),e);
        }
    }

    @Entonces("obtendra como respuesta {string} y {string}")
    public void obtendraComoRespuestaY(String currencyISOCode, String currencyName) {

        try {
            LOGGER.info("La respuesta obtenida es:\n "+LastResponse.received().answeredBy(theActor).prettyPrint());
            LOGGER.info("El content type es:\n"+LastResponse.received().answeredBy(theActor).getContentType());

            theActor.should(
                    seeThat(theResponseCodeIs(), Matchers.equalTo(HttpStatus.SC_OK)),
                    seeThat("El código ISO de la moneda es: ",theCountryCurrency(),Matchers.containsString(currencyName)),
                    seeThat("El nombre formal de la moneda es: ",theCountryCurrency(),Matchers.containsString(currencyISOCode))
            );

        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage(),e);
        }

    }
}
