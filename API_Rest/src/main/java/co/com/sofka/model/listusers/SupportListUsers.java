package co.com.sofka.model.listusers;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"url",
"text"
})
public class SupportListUsers {

@JsonProperty("url")
private String urlListUsers;
@JsonProperty("text")
private String textListUsers;
@JsonIgnore
private final Map<String, Object> additionalProperties = new HashMap<>();

@JsonProperty("url")
public String getUrlListUsers() {
return urlListUsers;
}

@JsonProperty("url")
public void setUrlListUsers(String urlListUsers) {
this.urlListUsers = urlListUsers;
}

@JsonProperty("text")
public String getTextListUsers() {
return textListUsers;
}

@JsonProperty("text")
public void setTextListUsers(String textListUsers) {
this.textListUsers = textListUsers;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}