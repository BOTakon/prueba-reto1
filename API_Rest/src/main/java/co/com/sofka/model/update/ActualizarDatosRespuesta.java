package co.com.sofka.model.update;

public class ActualizarDatosRespuesta {
	private String name;
	private String job;
	private String updatedAt;

	public String getName(){
		return name;
	}

	public String getJob(){
		return job;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}
}
