package co.com.sofka.util;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

import static co.com.sofka.util.PalabrasClaves.COMPLETENAME;
import static co.com.sofka.util.PalabrasClaves.JOB;


public class LeerArchivoJson {

    private static String fullName;
    private static String job;


    //Contructor
    public LeerArchivoJson(String job, String fullName) {
        LeerArchivoJson.job = job;
        LeerArchivoJson.fullName = fullName;
    }

    //lectura del archvio Json
    
    public static String convertPatch() throws IOException, ParseException {
        Object jsonObjc = new JSONParser().parse(new FileReader("src/test/resources/archivosjson/PatchCustomer.json"));
        JSONObject jsonObject = (JSONObject) jsonObjc;
        String fullJson = JSONObject.toJSONString(jsonObject);
        String saveFullName = fullJson.replace(COMPLETENAME.getValue(), fullName);
        return saveFullName.replace(JOB.getValue(), job);
    }

}
