package co.com.sofka.question;

import co.com.sofka.model.singleresource.ColorData;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class SingleResourceQuestion implements Question<ColorData> {

    @Override
    public ColorData answeredBy(Actor actor) {
        return SerenityRest.lastResponse().as(ColorData.class);
    }

    public static SingleResourceQuestion singleResourceQuestion(){
        return  new SingleResourceQuestion();
    }
}
