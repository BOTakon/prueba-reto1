package co.com.sofka.task;


import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

import static co.com.sofka.util.Dictionary.RESOURCE;
import static co.com.sofka.util.Dictionary.URL_BASE;

public class EliminarDatos implements Task {

    public static EliminarDatos eliminarDatos(){
        return Tasks.instrumented(EliminarDatos.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        SerenityRest.given()
              .relaxedHTTPSValidation()
                .baseUri(URL_BASE).log().all()
                .urlEncodingEnabled(false)
                .contentType(ContentType.JSON)
                .when()
                .delete(RESOURCE)
                .then();
    }
}
