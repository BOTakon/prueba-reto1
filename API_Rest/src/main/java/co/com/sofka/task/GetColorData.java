package co.com.sofka.task;

import io.restassured.specification.RequestSpecification;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;

public class GetColorData implements Task {

    private String resource;

    public GetColorData useTheResource(String resource) {
        this.resource = resource;
        return this;
    }
    public static GetColorData getColorData(){
        return new GetColorData();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Get.resource(resource).with(RequestSpecification::relaxedHTTPSValidation));
    }
}
