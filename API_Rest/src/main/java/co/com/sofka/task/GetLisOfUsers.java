package co.com.sofka.task;

import io.restassured.specification.RequestSpecification;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;

public class GetLisOfUsers implements Task {

    private String resource;

    public GetLisOfUsers usingTheResource(String resource) {
        this.resource = resource;
        return this;
    }
    public static GetLisOfUsers doGet(){
        return new GetLisOfUsers();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Get.resource(resource).with(RequestSpecification::relaxedHTTPSValidation));
    }
}
