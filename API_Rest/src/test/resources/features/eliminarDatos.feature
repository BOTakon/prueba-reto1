# language: es
Característica: eliminar datos
    yo como cliente registrado
    deseo eliminar datos
    para validar el funcionamiento de los servicios

    Escenario: eliminar datos
        Dado el usuario esta en la plataforma y desea eliminar datos
        Cuando el usuario realiza la peticion de eliminar datos
        Entonces el usuario obtendra un codigo de respuesta 204