package co.com.sofka.stepdefinitions;

import co.com.sofka.common.Setup;
import co.com.sofka.question.SingleResourceQuestion;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.hamcrest.Matchers;

import static co.com.sofka.task.GetColorData.getColorData;
import static co.com.sofka.util.Dictionary.SINGLE_RESOURCE;
import static co.com.sofka.util.Dictionary.URL_BASE;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;

public class SingleResourceStepDefinition extends Setup {


    private final Actor actor = Actor.named("DanielaG");
    private String bodyRequest;

    private static final Logger LOGGER = Logger.getLogger(SingleResourceStepDefinition.class);

    @Dado("que estoy en el servicio de paleta de colores")
    public void queEstoyEnElServicioDePaletaDeColores() {

        try{
            generalSetUp();
            actor.can(CallAnApi.at(URL_BASE));


        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

        }

    @Cuando("solicite el codigo de color fucsia")
    public void soliciteElCodigoDeColorFucsia() {

        try {
        actor.attemptsTo(getColorData().useTheResource(SINGLE_RESOURCE));


        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("se obtendra un status {int} y el codigo correspondiente")
    public void seObtendraUnStatusYElCodigoCorrespondiente(Integer statusCode) {

         String codigoColor = SingleResourceQuestion.singleResourceQuestion().answeredBy(actor).getData().getColor();

         actor.should(
                 seeThatResponse(
                         "El código de respuesta debe ser: " + statusCode,
                         validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_OK)),
                 seeThat(
                         "El codigo del color es",actor1 -> codigoColor, Matchers.notNullValue()
                 )

         );
    }
}
