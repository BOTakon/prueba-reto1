package co.com.sofka.stepdefinitions;

import co.com.sofka.common.Setup;
import co.com.sofka.model.update.ActualizarDatosRespuesta;
import co.com.sofka.model.update.CatosModelActualizarDatos;
import co.com.sofka.question.ActualizarDatosQuestion;
import co.com.sofka.util.LeerArchivoJson;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;

import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;

import static co.com.sofka.task.PutDatosUpdate.doPut;

import static co.com.sofka.util.Dictionary.RESOURCE;
import static co.com.sofka.util.Dictionary.URL_BASE;
import static co.com.sofka.util.Persona.generarPersonasRandom;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.equalTo;

public class PutDatosStepDefinition extends Setup {

    private final Actor actor = Actor.named("JohanM");
    private String bodyRequest;
    private CatosModelActualizarDatos datosRandom;

    private LeerArchivoJson archivoJson;
    private static final Logger LOGGER = Logger.getLogger(PutDatosStepDefinition.class);


    @Dado("se actualizaron los datos por put")
    public void seActualizaronLosDatosPorPut() {

        try{
            generalSetUp();
            actor.can(CallAnApi.at(URL_BASE));


            datosRandom = generarPersonasRandom();
            archivoJson = new LeerArchivoJson(datosRandom.getJob(), datosRandom.getFullName() );
            String SaveNameJob = archivoJson.convertPatch();
            bodyRequest = SaveNameJob;
            LOGGER.info(bodyRequest);

        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Cuando("se envia informacion request")
    public void seEnviaInformacionRequest() {

        try{

            actor.attemptsTo(
                    doPut()
                            .usingTheResource(RESOURCE)
                            .andBodyRequest(bodyRequest)
            );

        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);

        }

    }

    @Entonces("se obtendra confirmacion")
    public void seObtendraConfirmacion() {

        try{

            LastResponse.received().answeredBy(actor).prettyPrint();

            ActualizarDatosRespuesta Customer = new ActualizarDatosQuestion().answeredBy(actor);
            actor.should(
                    seeThatResponse(
                            "El código de respuesta debe ser: " + HttpStatus.SC_OK,
                            validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_OK)),
                    seeThat(
                            "El nombre del usuario es:", x -> Customer.getName(), equalTo(datosRandom.getFullName())),
                    seeThat(
                            "El trabajo del usuario es:", x -> Customer.getJob(), equalTo(datosRandom.getJob()))
            );

        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);

        }

    }


}
