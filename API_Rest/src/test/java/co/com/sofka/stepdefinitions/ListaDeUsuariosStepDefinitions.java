package co.com.sofka.stepdefinitions;

import co.com.sofka.common.Setup;
import co.com.sofka.model.listusers.Datum;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.hamcrest.Matchers;

import java.util.List;

import static co.com.sofka.question.TheListOfUsers.theListOfUsers;
import static co.com.sofka.task.GetLisOfUsers.doGet;
import static co.com.sofka.util.Dictionary.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;

public class ListaDeUsuariosStepDefinitions extends Setup {

    private final Actor actor = Actor.named("AlvaroD");
    private static final Logger LOGGER = Logger.getLogger(ListaDeUsuariosStepDefinitions.class);


    @Dado("que como administrador estoy el modulo de usuarios")
    public void queComoAdministradorEstoyElModuloDeUsuarios() {
        try{
            generalSetUp();
            actor.can(CallAnApi.at(URL_BASE));

        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }

    }

    @Cuando("solicite el listado de todos los usuarios")
    public void soliciteElListadoDeTodosLosUsuarios() {

        try{

            actor.attemptsTo(
                    doGet().usingTheResource(LIST_USERS)
            );

        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);

        }
    }

    @Entonces("podre ver la lista completa de usuarios")
    public void podreVerLaListaCompletaDeUsuarios() {
        try{

            List<Datum> theListOfUsers = theListOfUsers().answeredBy(actor).getData();

            actor.should(
                    seeThatResponse(
                            "El código de respuesta debe ser: " + HttpStatus.SC_OK,
                            validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_OK)),
                    seeThat("La lista de usuarios",act->theListOfUsers,Matchers.notNullValue())

            );

        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);

        }

    }
}
