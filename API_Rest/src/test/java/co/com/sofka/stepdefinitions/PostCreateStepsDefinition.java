package co.com.sofka.stepdefinitions;

import co.com.sofka.common.Setup;
import co.com.sofka.model.create.CreateRespuesta;
import co.com.sofka.model.update.CatosModelActualizarDatos;
import co.com.sofka.question.CreateQuestion;
import co.com.sofka.util.LeerArchivoJson;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.questions.LastResponse;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;

import static co.com.sofka.task.DoActualizarDatos.doPatch;
import static co.com.sofka.util.Dictionary.*;
import static co.com.sofka.util.Persona.generarPersonasRandom;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;

public class PostCreateStepsDefinition extends Setup {
    private final Actor actor = Actor.named("JhonS");
    private String bodyRequest;
    private CatosModelActualizarDatos datosRandom;
    private LeerArchivoJson archivoJson;
    private static final Logger LOGGER = Logger.getLogger(PostCreateStepsDefinition.class);

    @Dado("se crea nuevo puesto por post")
    public void seCreaNuevoPuestoPorPost(){
        try{
            generalSetUp();
            actor.can(CallAnApi.at(URL_BASE));
            datosRandom = generarPersonasRandom();
            archivoJson = new LeerArchivoJson(datosRandom.getJob(), datosRandom.getFullName());
            String SaveNameJob = archivoJson.convertPatch();
            bodyRequest = SaveNameJob;
            LOGGER.info(bodyRequest);
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Cuando("se solicita por request")
    public void seSolicitaPorRequest(){
        try{
            actor.attemptsTo(
                    doPatch()
                            .usingTheResource(CREATE)
                            .andBodyRequest(bodyRequest)
            );
        } catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Entonces("se obtiene respuesta 200 con id")
    public void seObtieneRespuesta200ConId(){
        try{
            LastResponse.received().answeredBy(actor).prettyPrint();
            CreateRespuesta User = new CreateQuestion().answeredBy(actor);
            actor.should(
                    seeThatResponse(
                            "La respuesta esperada es: " + HttpStatus.SC_CREATED,
                            validatableResponse -> validatableResponse.statusCode(HttpStatus.SC_CREATED)),
                    seeThat("El nombre el Usuario es: ", x -> User.getName(), equalTo(datosRandom.getFullName())),
                    seeThat("El trabajo del Usuario es: ", x -> User.getJob(), equalTo(datosRandom.getJob())),
                    seeThat("El ID del Usuario es: ", x -> User.getId(), notNullValue())
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }
}
