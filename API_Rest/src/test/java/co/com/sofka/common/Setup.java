package co.com.sofka.common;

import org.apache.log4j.PropertyConfigurator;

import static co.com.sofka.util.Log4jValues.*;

public class Setup {
    private String SO = System.getProperty("os.name").toLowerCase();

    protected void generalSetUp(){
        setUpLog4j2();
    }

    private void setUpLog4j2() {

        if (SO.contains("win")) {

            PropertyConfigurator.configure(USER_DIR.getValue() + LOG4J_PROPERTIES_FILE_PATH_WINDOWS.getValue());
        }
        else {
            PropertyConfigurator.configure(USER_DIR.getValue() + LOG4J_PROPERTIES_FILE_PATH_LINUX.getValue());
        }
    }
}
