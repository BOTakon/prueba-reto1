# Reto Grupal QA

# Alcance

Se probarán las siguientes aplicaciones:

- API: [https://reqres.in/](https://reqres.in/)
- SOAP: [http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso](http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso)
- WebUI: [https://demo.testim.io/](https://demo.testim.io/)

Se crearán 20 escenarios repartidos como sigue

| Aplicacion | No. de Escenarios |
| --- | --- |
| API | 7 |
| SOAP | 5 |
| WebUI | 8 |

**Fuera del alcance:** otros escenarios no especificados en el plan de pruebas.

# Estrategia de pruebas

- Se usará como gestor del repositorio remoto **Gitlab.**
- Se usará un modelo gitflow **Trunk Based Development.**
- Se utilizará el patrón **screenplay** con **SerenityBDD** y **Cucumber**. Las automatizaciones sopotarán ejecuciones en Windows y Linux. Se usará una máquina virtual en **Jenkins** con SO Linux/UNIX – Ubuntu 20.04. Las pruebas UI se harán usando google Chrome.
- Para el **CI/CD** se usarán las pipelines de Jenkins. Dentro del modelo se realizará lo siguiente: un pipeline (CI) que compile el código fuente que cada uno de los testers sube al repositorio remoto en cada rama *feature** del trunkbase; hay que asegurar que se ejecute un pipeline (o CD) que compile el código fuente que tester mezcla a la rama *main* mediante el repositorio remoto.
- Se usará **Sonarqube** como analizador de código estático y se integrará con el IDE **Intellij**.
- Para las pruebas, se generaran datos aleatorios para usuarios usando librerias de aleatorizacion de java, asi como **JAVA Faker** en donde se requiera.

## Escenarios API-Rest

- **Alvaro Ducuara**
    - PATCH
        
        `https://reqres.in/``api/users/2`
        
        ```gherkin
        # language: es
        Característica: Actualizar y eliminar datos
          yo como cliente registrado
          deseo Acrtualizar datos
          para validar el funcionamiento de los servicios
        
          Escenario: Actualizar datos
            Dado el usuario esta en la plataforma y desea actualizar datos
            Cuando el usuario logra actualizar los datos
            Entonces el usuario obtendra un codigo de respuesta exitosa y podra ver sus datos actualizados
        ```

   - **GET**
        
        ```gherkin
        # language: es
        Característica: obtener lista de usuarios
          como administrador
          deseo obtener la lista de usuarios
          para poder validar roles y permisos
        
          Escenario: obtener todos los usuarios
            Dado que como administrador estoy el modulo de usuarios
            Cuando solicite el listado de todos los usuarios
            Entonces podre ver la lista completa de usuarios
        ```
        
- **Samuel Duque**
    - DELETE
        
        ```gherkin
        # language: es
        Característica: eliminar datos
            yo como cliente registrado
            deseo eliminar datos
            para validar el funcionamiento de los servicios
        
            Escenario: eliminar datos
                Dado el usuario esta en la plataforma y desea eliminar datos
                Cuando el usuario realiza la peticion de eliminar datos
                Entonces el usuario obtendra un codigo de respuesta 204
        ```
        
- **Johan Muñetón**
    - PUT
        
        [https://reqres.in/api/users?page=2](https://reqres.in/api/users?page=2)
        
        ```gherkin
        # language: es
        Característica:Actualizacion Put
          como cliente registrado
          deseo actualizar por Put
          para validar   servicio
        
          Escenario: Actualizacion de datos por Put
            Dado se actualizaron los datos por put
            Cuando se envia informacion request
            Entonces se obtendra confirmacion
        ```
        
- **Esteban Londoño**
    
    POST LOGIN 
    
    ```gherkin
    # language: es
    Característica: Loguearse en el servicio
      yo como cliente registrado
      deseo ingresar al aplicativo
      para validar el funcionamiento de los servicios
    
      Escenario: Actualizar datos
        Dado el usuario esta en la plataforma y desea loguearse en el servicio
        Cuando el usuario logra loguearse con sus credenciales email "eve.holt@reqres.in" y password "cityslicka"
        Entonces el usuario obtendra un codigo de respuesta exitosa y podra ver su token de acceso
    ```
    
- **Daniela Grajales**
    - GET
        
         [SINGLE <RESOURCE>](https://reqres.in/api/unknown/2)
        
        ```gherkin
        # new feature
        # Tags: optional
        #language: es
        
        Característica: Paleta de colores
        como diseñador grafico
        quiero poder conocer el codigo hexadecimal
        para poder generar la paleta de colores de mi cartelera
        
          Escenario: Obtener el codigo de un solo color
            Dado que estoy en el servicio de paleta de colores
            Cuando solicite el codigo de color fucsia
            Entonces se obtendra un status 200 y el codigo correspondiente
        ```
        
- **Jhon Sarmiento**
    - POST
        
        [Create](https://reqres.in/api/users): [https://reqres.in/api/users](https://reqres.in/api/users)
        
        ```gherkin
        # language: es
        Característica:Creación Nombre y Trabajo
          Como Usuario
          Quiero crear un nuevo trabajador
          para añadir nuevo puesto
        
          Escenario: Creación por Post
            Dado se crea nuevo puesto por post
            Cuando se solicita por request
            Entonces se obtiene respuesta 200 con id
        ```
        

## Escenarios WebUI

- **Jorge Escorcia**
    - **Scenario Login Exitoso Y Login Fallido**
        
        ```gherkin
        #language: es
        
         Característica: funcionalidad Login
          Yo como usuario
          Quiero poder loguearme en la web
          Para  navegar dentro de ella
        
         Escenario: inicio de sesion exitoso
           Dado que el usuario se encuentra en la pagina de inicio de sesion
           Cuando el usuario ingresa un nombre de usuario y una contrasena validos
           Entonces el usuario debe tener acceso a la pagina de inicio
        
         Escenario: inicio de sesion fallido
           Dado que el usuario quiere iniciar sesion
           Cuando el usuario no ingresa un password
           Entonces el usuario espera ver un mensaje de autenticacion fallida
        ```
        
- **Esteban Londoño**
    - **aplicarCodigoDeDescuento**
        
        ```gherkin
        # language: es
        @FeatureName: aplicarCodigoDeDescuento
        
        Característica: Aplicar codigo de descuento
          Como usuario del aplicativo
          Quiero conocer los el precio de un viaje
          Para poder realizar análisis de dinámicas políticas inter y transcontinentales
        
          @ScenarioName: cotizacionDeViajeConDescuento
          Escenario: Cotizacion de viaje con descuento
            Dado que el usuario se encuentra en la pagina principal del aplicativo
            Cuando cotiza su viaje a "Shenji" y aplica el codigo "PROMO"
            Entonces debera ver que el precio del viaje sin descuento es de "1089.07"
            Y el precio con descuento sera de "871.26"
        ```
        
- **Samuel Duque**
    - **Escenario: consultar temperatura**
        
        ```gherkin
        # languague: es
        
        @FeatureName:ConocerTemperatura
        Característica: Conocer Temperatura
          Yo como usuario
          Quiero saber la temperatura del lugar
          Para realizar la reserva
        
          @ScenarioName:ConocerTemperatura  
        	Escenario: Conocer temperatura
            Dado que el usuario cotiza un viaje
            Cuando desea conocer las temperaturas del lugar
            Entonces el usuario vera el apartado de "TEMPERATURES"
        ```
        
- **Jesus Pacheco**
    - **Cambiar rango de precios**
        
        ```gherkin
        # language: es
        
        @FeatureName: Cambiarrangodeprecios
        
        Característica:Cambiar rango de precios
          Como cliente interesado en adquirir un viaje
          necesito poder escoger el rango de precios
          para poder encontrar un viaje acorde a mi presupuesto
        
          @ScenarioName: Disminuirrangodeprecios
          Escenario: Disminuir rango de precios
            Dado Que el usuario se encontraba en la pagina principal
            Cuando Redujo el rango de precios para los viajes
            Entonces encuentra un viaje ajustado a su presupuesto
        ```
        
- **Daniela Grajales**
    - **Iniciar sesion y cerrar sesion**
        
        ```gherkin
        # language: es
        
        @FeatureName: Iniciar sesion y cerrar sesion
        
        Característica: Iniciar sesion y cerrar sesion
          yo como usuario deseo poder ingresar a la plataforma
          y luego de haber interactuado con la plataforma
          poder cerrar sesion
        
          @ScenarioName: Iniciar sesion y cerrar sesion
          Escenario: Disminuir rango de precios
        	  Dado que el usuario desea iniciar sesion
            Cuando ya termina de interactuar con la pagina desea cerrar sesion
            Entonces el usuario podra iniciar sesion y cerrar sesion cuando lo desee
            
        ```
        
- **Jhon Sarmiento**
    - **Escenario: filtro de destinos por color**
        
        ```gherkin
        # language: es
        @FeatureName: filtrarColor
        
        Característica: Realizar filtro por color
          Como usuario de la pagina
          Quiero filtrar los destinos por color
          Para ver mis destinos preferidos
        
          Escenario: Filtrar destinos
            Dado que el usuario ingresa a la pagina principal
            Cuando filtra por color purpura
            Entonces se mostrara el destino Shaheying
        ```
        
- **Johan Muñetón**
    - **elegir un viaje del contenido oculto**
        
        ```gherkin
        # language: es
        Característica: funcionalidad para ver opciones
          Como usuario en la web
          Quiero desplegar las opciones  Para  verlas en su totalidad
        
          Escenario: elegir un viaje del contenido oculto 
           Dado El usuario estaba logueado en la pagina web
            Cuando El usuario accede al contenido del boton cargar mas para el egir
            Entonces El usuario vera el ultimo destino
        
        ```
        

## Escenarios SOAP

- **Jesús Castellanos**
    
    ```gherkin
    #language: es
    @Feature: InformacionDelPais
      Característica: Informacion del pais
      Como usuario de la pagina DataFlex
      necesito consultar con el código ISO del pais
      para poder obtener la informacion completa de un pais.
    
      @ScenarioName: CódigoIsoVerificado
        Dado que el usuario necesita consultar la información completa de un pais, con el código ISOCOL
    Cuando el usuario de ejecuta una consulta
        Entonces el ususario debería obtener toda la informacion del pais
    @ScenarioName: CódigoIsoNoValido
        Dado que el usuario ingresa un código ISO no validoFALSE
    Cuando el usuario realiza una consulta
        Entonces el ususario debería obtener como respuesta: Country not found in the database
    ```
    
- **Jose Velasco**
    
    Servicio: **CurrencyName**
    
    ```gherkin
    # language: es
    @FeatureName: monedaDelPais
    **Caracteristica:** Moneda del pais
      **Como** inversionista de negocios internacionales
      **Deseo** saber la moneda que se usa en un determindado pais
      **Para poder** calcular la tasa de cambio del valor de mis importaciones y exportaciones
    
    	@ScenarioName: codigoISOYNombreDeMoneda
    	**Escenario:** obtener codigo ISO y nombre de moneda
    	  **Dado** que el inversionista paso el codigo ISO del pais Colombia como “CO” al recurso
    	  **Cuando** solicite el codigo ISO y el nombre de la moneda de ese pais
    	  **Entonces** obtendra como respuesta “COP"y "Pesos”
    ```
    
    Servicio: **ListOfContinentsByName**
    
    ```
    # language: es
    @FeatureName: nombresDeContinentes
    **Característica:** Obtener nombres de continentes
      **Como** analista geopolítico
      **Quiero** conocer los nombres y códigos ISO de los continentes
      **Para** poder realizar análisis de dinámicas políticas inter y transcontinentales
     
    	@ScenarioName: listarNombresDeContinentes
    	**Escenario:** Listar nombres y códigos ISO de continentes
    	  **Dado** que el analista geopolítico ha ingresado al recurso de listar nombres
    	  **Cuando** solicite el listado de nombres de los continentes
    	  **Entonces** obtendra el listado de los codigos ISO y nombres de los continentes
    ```
    
- **Jesús Pacheco**
    
    ```gherkin
    # language: es
    
    @FeatureName:Capitalesdelmundo
      Característica: Capitales del mundo
        Como Cartografo del mundo
        deseo poder conocer las capitales principales
        para poder tener un conocimiento más amplio
    
      @ScenarioName:EncontrarCapitales
        Escenario: Encontrar Capitales
          Dado  El Cartografo estaba ubicado en la pagina
          Cuando ingresa el Iso Code de un pais
          Entonces recibe en pantalla la capital de dicho pais
    
    ```
    

# Otros tipos de pruebas

Se recomienda al equipo realizar las siguientes pruebas:

- Pruebas de Rendimiento
- Pruebas de Seguridad

# Requerimientos

Acceso a ambiente de desarrollo

Acceso a gestor de repositorios remoto Gitlab

Acceso a framework de automatizacion de Sofka Tech con sus credenciales válidas

Acceso a ambientes de maquina virtual y pipelines Jerkins

Acceso a analizador de codigo estático Sonarqube