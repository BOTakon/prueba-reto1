package co.com.sofka.userinterface.temperatura;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

import static org.openqa.selenium.By.*;

public class Temperatura extends PageObject {

    public static final Target BOOK = Target
            .the("BOOK")
            .located(xpath("//*[@id='app']/div/section[2]/div[4]/div/div/div[1]/div[4]/button"));
/*
    public static final Target NAME = Target
            .the("NAME")
            .located(xpath("//*[@id='app']/div/div[2]/section[1]/div[3]/div[1]/form/div[1]/input"));

    public static final Target EMAIL = Target
            .the("EMAIL")
            .located(xpath("//*[@id='app']/div/div[2]/section[1]/div[3]/div[1]/form/div[2]/input"));

    public static final Target SSN = Target
            .the("SSN")
            .located(xpath("//*[@id='app']/div/div[2]/section[1]/div[3]/div[1]/form/div[3]/input"));

    public static final Target PHONE_NUMBER = Target
            .the("PhoneNumber")
            .located(xpath("//*[@id='app']/div/div[2]/section[1]/div[3]/div[1]/form/div[4]/input"));

    public static final Target PROMO_CODE = Target
            .the("PromoCode")
            .located(xpath("//*[@id='app']/div/div[2]/section[1]/div[3]/div[2]/div[4]/div[1]/div/input"));

    public static final Target PAY_BTN = Target
            .the("PayBTN")
            .located(xpath("//*[@id='app']/div/div[2]/section[1]/div[3]/div[2]/div[7]/div/button"));



    //For validations.

    public static final Target TERMS_AND_CONDITIONS_ALERT = Target
            .the("TermsAndContiditonsValidation")
            .located(xpath("/html/body/div[2]/div/div[2]/section/h6"));*/

    public static final Target TEMPERATURES =
            Target.the("Temperatures").located(By.xpath("//h1[@class='Climate__headline-1___pmwAe']"));


}
