package co.com.sofka.question.filtro;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import static co.com.sofka.userinterface.filtro.Filtro.SHAHEYING;

public class FiltroQuestion implements Question<Boolean> {
    public Boolean answeredBy(Actor actor) {
        return (SHAHEYING.resolveFor(actor).containsOnlyText("Shaheying"));
    }
    public static FiltroQuestion filtroQuestion(){return new FiltroQuestion();}
}
