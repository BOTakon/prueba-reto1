package co.com.sofka.question.loginandsignout;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.userinterface.loginandsignout.SignOut.BUTTON_LOGIN;
import static co.com.sofka.util.Dictionary.LOGO_MESSAGE_LOG_OUT;

public class LoginAndSignOutError implements Question<Boolean> {

    @Override
    public Boolean answeredBy(Actor actor) {
        return BUTTON_LOGIN
                .resolveFor(actor).containsOnlyText(LOGO_MESSAGE_LOG_OUT);
    }

    public static LoginAndSignOutError loginAndSignOutError() {
        return new LoginAndSignOutError();
    }

    public LoginAndSignOutError is() {
        return this;
    }
}
