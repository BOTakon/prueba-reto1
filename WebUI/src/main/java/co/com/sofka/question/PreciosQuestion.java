package co.com.sofka.question;

import co.com.sofka.userinterface.precios.PreciosPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class PreciosQuestion implements Question<String> {

    public static PreciosQuestion preciosQuestion() {
        return new PreciosQuestion();
    }

    @Override
    public String answeredBy(Actor actor) {
        return Text.of(PreciosPage.TEXT_CONFIRM).viewedBy(actor).asString();
    }
}
