package co.com.sofka.question.temperatura;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static co.com.sofka.userinterface.temperatura.Temperatura.*;

public class CheckTemperatura implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return Text.of(TEMPERATURES).viewedBy(actor).asString();
    }
    public static CheckTemperatura checkAparadoTemperatura(){
        return new CheckTemperatura();
    }
}
