package co.com.sofka.question;

import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.TextContent;

public class ResumeLogin {
    private ResumeLogin() { }

    public static Question<String> message(){
        return actor -> TextContent.of(Mensaje.message).viewedBy(actor).asString().trim();
    }
    public static Question<String> messageLoginFailed(){
        return actor -> TextContent.of(Mensaje.messageLoginFailed).viewedBy(actor).asString().trim();
    }
}
