package co.com.sofka.task.temperatura;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;

import static co.com.sofka.userinterface.temperatura.Temperatura.BOOK;

public class CotizarViaje implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Scroll.to(BOOK),
                Click.on(BOOK)
        );
    }

    public static CotizarViaje cotizarViajeViaje(){
        return new CotizarViaje();
    }
}
