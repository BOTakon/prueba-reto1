package co.com.sofka.task.loadmorebutton;


import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.sofka.userinterface.loadmorebutton.LoadMoreButton.*;


public class NavegateToLoadMore implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(

                Click.on(LOAD_MORE),
                Click.on(CUOZHOU),
                Click.on(BOOK_BUTTON),
                Click.on(TEMP_MESSAGE)
                );
    }
    public static NavegateToLoadMore navegateToLoadMore(){
        return new NavegateToLoadMore();
    }


}
