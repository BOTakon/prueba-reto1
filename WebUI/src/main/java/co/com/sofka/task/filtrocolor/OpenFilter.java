package co.com.sofka.task.filtrocolor;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;

import static co.com.sofka.userinterface.filtro.Filtro.*;

public class OpenFilter implements Task {

    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Scroll.to(LIST),
                Click.on(LIST),
                Scroll.to(PURPLE_COLOR),
                Click.on(PURPLE_COLOR)
        );
    }
    public static OpenFilter openFilter(){return new OpenFilter();}
}
