package co.com.sofka.task.temperatura;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Scroll;


import static co.com.sofka.userinterface.temperatura.Temperatura.*;


public class VerTemperatura implements Task {



    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Scroll.to(TEMPERATURES)

        );

    }
    public static VerTemperatura verTemperatura(){
        return new VerTemperatura();
    }
}
