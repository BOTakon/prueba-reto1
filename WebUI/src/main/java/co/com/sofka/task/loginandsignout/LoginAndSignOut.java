package co.com.sofka.task.loginandsignout;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;

import static co.com.sofka.userinterface.loginandsignout.SignOut.HELLO;
import static co.com.sofka.userinterface.loginandsignout.SignOut.SIGN_OUT;

public class LoginAndSignOut implements Task {



    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Scroll.to(HELLO),
                Click.on(HELLO),
                Scroll.to(SIGN_OUT),
                Click.on(SIGN_OUT)
        );
    }

    public static LoginAndSignOut loginAndSignOut(){
        return new LoginAndSignOut();
    }


    public LoginAndSignOut is()
    {
        return this;
    }
}
