package co.com.sofka.stepsdefinitions;

import co.com.sofka.setup.Setup;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;

import static co.com.sofka.question.temperatura.CheckTemperatura.checkAparadoTemperatura;
import static co.com.sofka.task.temperatura.CotizarViaje.cotizarViajeViaje;
import static co.com.sofka.task.landingpage.OpenLandingPage.openLandingPage;
import static co.com.sofka.task.temperatura.VerTemperatura.verTemperatura;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.containsString;

public class TemperaturaStepDefinition extends Setup {

    private static final String ACTOR_NAME = "User";


    @Dado("que el usuario cotiza un viaje")
    public void queElUsuarioCotizaUnViaje() {
        actorSetupTheBrowser(ACTOR_NAME);
        theActorInTheSpotlight().attemptsTo(
                openLandingPage(),
                cotizarViajeViaje()
        );

    }

    @Cuando("desea conocer las temperaturas del lugar")
    public void deseaConocerLasTemperaturasDelLugar() {
        theActorInTheSpotlight().attemptsTo(
                verTemperatura()
        );
    }

    @Entonces("el usuario vera el apartado de {string}")
    public void elUsuarioVeraElApartadoDe(String messageExpected) {
        theActorInTheSpotlight().should(seeThat(checkAparadoTemperatura(), containsString(messageExpected)));


    }


}
