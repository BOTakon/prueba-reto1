package co.com.sofka.stepsdefinitions;


import co.com.sofka.question.PreciosQuestion;
import co.com.sofka.setup.Setup;
import co.com.sofka.task.preciostask.PreciosTask;
import com.github.javafaker.Faker;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;

import static co.com.sofka.task.landingpage.OpenLandingPage.openLandingPage;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.*;

public class PreciosStepDefinitions extends Setup {

    private static final String ACTOR_NAME = "Cliente";
    Faker faker = new Faker();

    @Dado("Que el usuario se encontraba en la pagina principal")
    public void queElUsuarioSeEncontrabaEnLaPaginaPrincipal() {
    actorSetupTheBrowser(ACTOR_NAME);
    theActorInTheSpotlight().attemptsTo(
            openLandingPage()
    );


    }

    @Cuando("Redujo el rango de precios para los viajes")
    public void redujoElRangoDePreciosParaLosViajes() {

        theActorInTheSpotlight().attemptsTo(
                PreciosTask.pruebatask()
                        .presupuesto(String.valueOf(faker.number().numberBetween(200,1800)))
        );

    }

    @Entonces("encuentra un viaje ajustado a su presupuesto")
    public void encuentraUnViajeAjustadoASuPresupuesto() {

       theActorInTheSpotlight().should(
               seeThat(PreciosQuestion.preciosQuestion(), notNullValue()
               )
       );

    }
}
