package co.com.sofka.stepsdefinitions;

import co.com.sofka.setup.Setup;
import com.github.javafaker.Faker;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import static co.com.sofka.question.filtro.FiltroQuestion.filtroQuestion;
import static co.com.sofka.task.filtrocolor.OpenFilter.openFilter;
import static co.com.sofka.task.landingpage.OpenLandingPage.openLandingPage;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class FiltrarColorStepsDefinitions extends Setup {

    private static final String ACTOR_NAME = "Jhon";
    Faker faker = new Faker();

    @Dado("que el usuario ingresa a la pagina principal")
    public void queElUsuarioIngresaALaPaginaPrincipal(){
        actorSetupTheBrowser(ACTOR_NAME);
        theActorInTheSpotlight().wasAbleTo(
                openLandingPage());
    }

    @Cuando("filtra por color purpura")
    public void filtraPorColorPurpura(){
        theActorInTheSpotlight().wasAbleTo(
                openFilter());
    }

    @Entonces("se mostrara el destino Shaheying")
    public void seMostraraElDestinoShaheying(){
        theActorInTheSpotlight().asksFor(filtroQuestion());
    }
}
