package co.com.sofka.stepsdefinitions;

import co.com.sofka.setup.Setup;
import co.com.sofka.question.ResumeLogin;
import com.github.javafaker.Faker;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;


import static co.com.sofka.task.landingpage.OpenLandingPage.openLandingPage;
import static co.com.sofka.task.login.LlenarCamposLogin.llenarCamposLogin;
import static co.com.sofka.task.login.NavegateToLogin.navegateToLogin;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.core.IsEqual.equalTo;

public class LoginSteps extends Setup {

    Faker faker = new Faker();
    private static final String ACTOR_NAME = "Student";

    @Dado("que el usuario se encuentra en la pagina de inicio de sesion")
    public void que_el_usuario_se_encuentra_en_la_pagina_de_inicio_de_sesion() {
        actorSetupTheBrowser(ACTOR_NAME);
        theActorInTheSpotlight().wasAbleTo(
                openLandingPage(),
                navegateToLogin()

        );

    }




    @Cuando("el usuario ingresa un nombre de usuario y una contrasena validos")

    public void el_usuario_ingresa_un_nombre_de_usuario_y_una_contrasena_validos() {
        theActorInTheSpotlight().wasAbleTo(
                llenarCamposLogin()
                        .emailAddressLogin(faker.internet().emailAddress())
                        .passwordLogin(faker.internet().password())


        );

    }

    @Entonces("el usuario debe tener acceso a la pagina de inicio")
    public void el_usuario_debe_tener_acceso_a_la_pagina_de_inicio() {
        theActorInTheSpotlight().should(
                seeThat( "el mensaje de inicio es ", ResumeLogin.message() , equalTo("Hello, John"))
        );

    }

    @Dado("que el usuario quiere iniciar sesion")
    public void que_el_usuario_quiere_iniciar_sesion() {
        actorSetupTheBrowser(ACTOR_NAME);
        theActorInTheSpotlight().wasAbleTo(
                openLandingPage(),
                navegateToLogin()

        );

    }

    @Cuando("el usuario no ingresa un password")
    public void el_usuario_no_ingresa_un_password() {
        theActorInTheSpotlight().wasAbleTo(

                llenarCamposLogin()
                        .emailAddressLogin(faker.internet().emailAddress())
                        .passwordLogin("")
        );

    }

    @Entonces("el usuario espera ver un mensaje de autenticacion fallida")
    public void el_usuario_espera_ver_un_mensaje_de_autenticacion_fallida() {
        theActorInTheSpotlight().should(
                seeThat( "el mensaje de error es ", ResumeLogin.messageLoginFailed() , equalTo("Password is a required field."))
        );

    }


}
