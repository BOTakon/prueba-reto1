# language: es
@FeatureName:ConocerTemperatura
Característica: Conocer Temperatura
  Yo como usuario
  Quiero saber la temperatura del lugar
  Para realizar la reserva

  @ScenarioName:ConocerTemperatura
  Escenario: Conocer temperatura
    Dado que el usuario cotiza un viaje
    Cuando desea conocer las temperaturas del lugar
    Entonces el usuario vera el apartado de "TEMPERATURES"

