# language: es
@FeatureName: filtrarColor

Característica: Realizar filtro por color
  Como usuario de la pagina
  Quiero filtrar los destinos por color
  Para ver mis destinos preferidos

  Escenario: Filtrar destinos
    Dado que el usuario ingresa a la pagina principal
    Cuando filtra por color purpura
    Entonces se mostrara el destino Shaheying